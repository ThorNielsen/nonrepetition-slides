\documentclass{beamer}
\usepackage{geometry}
\geometry{a4paper,papersize={14cm, 11.5cm}}
\usepackage[utf8]{inputenc}
\PassOptionsToPackage{usenames,dvipsnames,svgnames,table}{xcolor}
\usepackage[british]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{float}
\usepackage{centernot}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage{bbm}
\usepackage{halloweenmath}
\usepackage{listings}
\usepackage{tikz}
\usepackage{pgfplots}

\usepackage{hyperref}
\hypersetup{%
pdfauthor = {Thor Gabelgaard Nielsen},
pdfborder = {0 0 0},
pdfcreator = {},
pdfproducer = {},
pdfstartpage = {1},
pdfsubject = {},
pdftitle = {}}

\usepackage{xcolor}
\usepackage{pgfornament}

\author{Thor Gabelgaard Nielsen}
\title{XM: Game of non-repetition}
\date{23rd January 2018}

\newcommand{\nimplies}{\centernot\implies}
\newcommand{\niton}{\centernot\ni}
\newcommand{\qedsymb}{\ensuremath{\blacksquare}}
\newcommand{\numberhere}{\stepcounter{equation}\tag{\theequation}}

\newlength{\listingindent}
\setlength{\listingindent}{1mm}
\font\tt=rm-lmtl10
\font\itt=rm-lmtlo10
\font\btt=rm-lmtk10
\font\bitt=rm-lmtko10
\renewcommand{\thelstnumber}
{
    \protect{\tt\arabic{lstnumber}}
}

\lstdefinestyle{cstyle}
{
    basicstyle=\tt,
    breaklines=true,
    captionpos=b,
    commentstyle=\color{gray},
    extendedchars=true,
    frame=l,
    identifierstyle=\color{black},
    keywordstyle=\btt\color[HTML]{002CAE},
    language=c++,
    morekeywords={function, from, to, step, in},
    numbers=left,
    stringstyle=\color[HTML]{0037FF},
    showstringspaces=false,
    xleftmargin=2\listingindent,
    xrightmargin=1\listingindent,
}
\newcommand{\code}[1]{\mbox{\tt #1}}
\newcommand{\keyword}[1]{{\btt\color[HTML]{002CAE}#1}}

\newcommand\plr[1]{\textsl #1}
\newcommand\sym[1]{\code{#1}}
\newcommand\cpp{{\color[HTML]{001029}\btt C\btt++}}
\usetikzlibrary{matrix,positioning,arrows}
\tikzset{
table/.style={
matrix of nodes,
column sep=-\pgflinewidth, row sep=-\pgflinewidth,
nodes={rectangle, draw=black,text width=2.5 ex, align=center},
text depth=0.5ex,
text height=2ex,
nodes in empty cells
}}

\subtitle{}

\usetheme{default}
\usecolortheme{seahorse}
\begin{document}
\frame{\titlepage}

\begin{frame}
\frametitle{Description of the game}
\begin{itemize}
\item<1-> We are studying a game of non-repetition which for a given parameter $n\in\mathbb N$ and a finite set of symbols $\{0, 1, \dots, s-1\}$ works as follows:
\item<2-> An empty sequence is constructed.
\item<3-> Two players \plr A and \plr B alternately place a symbol at the end of the sequence.
\item<4-> Whenever a player causes a repetition of length $n$ to occur, that player has lost and the other player won.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{About winning strategies}
\begin{itemize}
\item<1-> A deterministic strategy can be viewed as a function
\begin{equation*}
f:\{0,1,\dots,s-1\}^*\times\mathbb N \to \{0,1,\dots,s-1\}.
\end{equation*}
\item<2-> A winning strategy is a strategy which causes the player using that to win, no matter what the other player does.
\item<3-> We can also talk about winning strategies given some constraints.
\item<4-> For this particular game, Zermelo's theorem gives that one of the players must have a winning strategy.
\begin{itemize}
\item<5-> Can we identify the player with the winning strategy?
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Identifying players with winning strategies}
\begin{itemize}
\item<1-> A (very) coarse lower bound of the game length is $n$.
\item<2-> An upper bound of the game length is $s^n+n-1$.
\item<3-> The number of possible games is therefore greater than $s^n$ but smaller than $s^{s^n+n-1}$.
\item<4-> Brute force is only viable for very small $n$, other approaches must be used for greater $n$.
\item<5-> \plr B has a winning strategy for odd $n$ where $s=2$, which works by placing the opposite of the opponent's symbol.
%\begin{itemize}
%\item<5-> One option is to simulate random games.
%\item<6-> Another is to fix the strategy of one player and analysing all the possible games stemming from the other's choices.
%\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Brute force}
%\framesubtitle{In pseudocode}
\begin{itemize}
\item<1-> We first try brute force for as long as we can.
\end{itemize}
\pause
\begin{lstlisting}[style=cstyle]
function whoHasWinningStrategy(prefix, n, s)
    current = determineCurrentPlayer(prefix)
    if gameEnded(prefix, n)
        return current
    for symbol in {0, ..., s-1}
        if whoHasWinningStrategy(prefix + symbol) = current
            return current
    return not current
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Brute force}
\framesubtitle{Results}
\begin{table}
\centering
\begin{tabular}{l|cccccc}
& \multicolumn{6}{c}{$s=2$}\\\hline
Word length $n$&1&2&3&4&5&6\\
Winning strategy held by&\plr B&\plr B&\plr B&\plr A&\plr B&\plr A\\
$\approx \log_2$ of upper bound&2&5&10&19&36&69\\
\end{tabular}
\end{table}
\begin{table}
\begin{tabular}{l|ccc|cc|cc}
& \multicolumn{3}{|c}{$s=3$}&\multicolumn{2}{|c}{$s=4$}&\multicolumn{2}{|c}{$s=5$}\\\hline
Word length $n$&1&2&3&1&2&1&2\\
Winning strategy held by&\plr A&\plr B & \plr A&\plr B&\plr B&\plr A&\plr B\\
$\approx \log_2$ of upper bound&3&11&32&6&24&8&42
\end{tabular}
\end{table}
\end{frame}

\begin{frame}[fragile]
\frametitle{Brute force}
\framesubtitle{Runtime for $s=2$}
%\begin{itemize}
%\item The runtime is significantly faster than expected; $n=6$ finishes in a few seconds.
%\item Important: Although the code looks slower than the upper bound that is because we did not consider any constants in our derivation of the upper bound.
%\item When we take the logarithm of 
%\end{itemize}
\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[xlabel={$n$},ylabel={$\log_2(\log_2(\text{cycles}))$},ymin=0, ymax=7, xmin=0, xmax=7]
\addplot [domain=0:10, samples=2, only marks,color=red](1+x,2.93046);
\addplot [domain=0:10, samples=2, only marks,color=red](2+x,3.29832);
\addplot [domain=0:10, samples=2, only marks,color=red](3+x,3.78355);
\addplot [domain=0:10, samples=2, only marks,color=red](4+x,4.03851);
\addplot [domain=0:10, samples=2, only marks,color=red](5+x,4.60807);
\addplot [domain=0:10, samples=2, only marks,color=red](6+x,5.06115);
\addplot [domain=0:7, samples=2](x,0.42393*x+2.46958);

\addplot [domain=0:10, samples=2, only marks,color=blue](1+x,1);
\addplot [domain=0:10, samples=2, only marks,color=blue](2+x,2.32193);
\addplot [domain=0:10, samples=2, only marks,color=blue](3+x,3.32193);
\addplot [domain=0:10, samples=2, only marks,color=blue](4+x,4.24793);
\addplot [domain=0:10, samples=2, only marks,color=blue](5+x,5.16993);
\addplot [domain=0:10, samples=2, only marks,color=blue](6+x,6.10852);
%\addplot [domain=0:7, samples=2](x,1.0036*x+0.19378);

\addplot [domain=1:6, samples=6, only marks, color=green](x, {ln(x)/ln(2)});
\end{axis}
\end{tikzpicture}
\caption{Red is observed runtime, blue is calculated from the upper bound (with constants disregarded) and green is the lower bound (also disregarding constants). 
Fit: $\lg(\lg(c)) = 0.42393\cdot n+2.46958$.}
\end{figure}

% It looks like our algorithm is exponential in the game length (linear in ~log(game-length)).
\end{frame}

\begin{frame}
\frametitle{Brute force}
\framesubtitle{Runtime for $s=2$}
\begin{table}
\begin{tabular}{lcccccc}
$n$&1&2&3&4&5&6\\
Runtime&$56.34$ns&261.4ns&$3.994\mu\text{s}$&$25.28\mu\text s$&6.270ms&3.206s
\end{tabular}
\caption{Observed runtimes.}
\end{table}
\begin{itemize}
\item<1-> The runtime is significantly faster than the upper bound predicts; $n=6$ finishes in a few seconds.
\item<2-> It also seems to grow much faster than the lower bound.
\item<3-> Extrapolating from the fit of our observed runtimes we would expect about $2^{43.3}$ cycles to brute force the case where $n=7$.
\begin{itemize}
\item<4-> This corresponds to roughly $3145$ seconds on my computer.
\item<5-> I ran it for one and a half hour ($5393.18s$) without any result.
\end{itemize}
\item<6-> Extrapolating gives about $2^{58}$ cycles for checking $n=8$. This is equivalent to almost three years on my computer. 
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Trying other approaches}
\begin{itemize}
\item<1-> Brute force is clearly infeasible, so we fix a strategy for one of the players.
% When testing, we fix the strategy for one player and consider all possible moves the other player can make.
\begin{itemize}
\item<2-> We also restrict ourselves to $s=2$.
\item<2-> We only consider deterministic strategies.%A (deterministic) strategy is a function $f:\{0,1\}^* \times \mathbb N \to \{0,1\}$.
% Random strategies are NOT considered due to us knowing that there exists a winning deterministic strategy
% Using only {0,1} also has the nice property that saying "not something" gives a unique symbol.
\end{itemize}
\item<3-> This significantly cuts down the search space.% as we now only has to brute force one of the player's moves.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Testing strategies}
\framesubtitle{In pseudocode}
\begin{itemize}
\item<1-> We use the following code:
\begin{lstlisting}[style=cstyle]
function proveStrategy(game, n, strategy, player)
    current = determineCurrentPlayer(game)
    if gameEnded(game, n)
        return player = curent
    if player = current
        move = strategy(game, n)
        return proveStrategy(game + move, n, strategy, player)
    for symbol in {0, 1}
        if not proveStrategy(game + symbol, n, strategy, player)
            return false
    return true
\end{lstlisting}
\item<2-> We can test it since we know a winning strategy for \plr B for odd $n$.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Testing strategies}
\framesubtitle{Runtime of the flipping strategy for \plr B}
\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[xlabel={$n$},ylabel={$\log_2(\log_2(\text{cycles}))$},ymin=0, ymax=6, xmin=0, xmax=11]
\addplot [domain=0:12, samples=2, only marks,color=red](1+x, 2.91115876033575);
\addplot [domain=0:12, samples=2, only marks,color=red](3+x, 3.54310829921209);
\addplot [domain=0:12, samples=2, only marks,color=red](5+x, 4.04003834022349);
\addplot [domain=0:12, samples=2, only marks,color=red](7+x, 4.50580559739663);
\addplot [domain=0:12, samples=2, only marks,color=red](9+x, 5.05189081494);
\addplot [domain=0:11, samples=2](x,0.262208*x+2.699360);
\addplot [domain=0:12, samples=2, only marks,color=blue](2+x, 2.72440487459416);
\addplot [domain=0:12, samples=2, only marks,color=blue](4+x, 2.85794532669523);
\addplot [domain=0:12, samples=2, only marks,color=blue](6+x, 2.92713432520959);
\addplot [domain=0:12, samples=2, only marks,color=blue](8+x, 2.99123837083148);
\addplot [domain=0:12, samples=2, only marks,color=blue](10+x, 3.02179168380613);
%\addplot [domain=0.01:11, samples=128](x,{0.1866938*ln(x)+2.596338});
\end{axis}
\end{tikzpicture}
\caption{Time usage of testing the flipping strategy for \plr B. The red points signify where it is a winning strategy and the blue where it is not. Fit: $\lg(\lg(c)) = 0.262208*n+2.699360$}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Testing strategies}
\framesubtitle{Observations}
\begin{itemize}
\item<1-> Winning strategies takes longer to test than non-winning ones.
\begin{itemize}
\item<2-> Time usage depends on how quickly we can find a counterexample (if any).
\end{itemize}
% This makes sense -- it takes longer to exhaust all possible cases instead of returning early.
% This also means that the better a strategy is the higher runtime. High runtime = high confidence the strategy is good/winning.
% Randomisation can help to quickly find cases where the strategy doesn't work.
\item<3-> Extrapolating from the tests of the winning strategies we would expect it to take $2^{40}$ cycles to test a winning strategy for $n=10$, which is on the border of feasibility.
% Due to the fit having some uncertainties, expected time for 2^40 cycles is 5 minutes.
\begin{itemize}
\item<4-> The number of possible games depends on the strategy used.
\end{itemize}
% As we'll see an example of next frame, where we count all the cases that arise and must be examined.
% (Some winning strategies causes many more cases to arise than others).
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Constructing some strategies}
\framesubtitle{In pseudocode}
\begin{lstlisting}[style=cstyle, caption={Some different strategies that were used.}]
function flipStrategy(game, n)
    if length(game) = 0: return 0
    return not game[-1]

function prevXorStrategy(game, n)
    if length(game) < 2: return 0
    return game[-2] xor game[-1]

function alternateStrategy(game, n)
    if length(game) < 2: return 0
    return not game[-2];
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Examining the performance of strategies}
\begin{table}
\resizebox{\textwidth}{!}{%
\begin{tabular}{ll|ccc|ccc}
Player &&\multicolumn{3}{c}{\plr A}&\multicolumn{3}{|c}{\plr B}\\
Strategy&$n$&Wins&Total&Percentage&Wins&Total&Percentage\\\hline
Flip&4&10&24&41.6667\%&4&26&15.3846\%\\
Flip&6&63&227&27.7533\%&18&236&7.62712\%\\\hline
XOR&4&7&18&38.8889\%&16&31&51.6129\%\\
XOR&6&437&631&69.2552\%&507&839&60.4291\%\\\hline
Alternate&4&47&50&94\%&33&53&62.2642\%\\
Alternate&6&1954&1994&97.994\%&1510&2034&74.238\%\\\hline
Alternate*&4&56&59&94.9153\%&90&113&79.646\%\\
Alternate*&6&42798&46332&92.3724\%&423763&483652&87.6173\%\\
\end{tabular}}
\caption{Win statistics for some selected strategies and $n$. Starred versions avoid playing a losing move.}
\end{table}
% Important observations:
% One strategy performing better for B than A does not mean B has a winning strategy.
% Modifying a strategy such that it only places a losing move when forced to do so actually makes them less efficient.
%  - We would however still think that the other strategy is better -- after all, shouldn't we always try to win a specific game.
%  - If we play against a random opponent, then it may be stupid to avoid playing losing moves if you play many games.
%  - If we play against a real opponent, then it may be beneficial to use the starred version even though it wins slightly less of the time.
%  - The number of possible games may also explode.
% Why couldn't I do this for n=8? Because early-out optimisations are removed, so it goes through ALL the different possible games, which may be extremely high. Just for n=6, one of the strategies took several seconds to test all the possible games while collecting information.
\end{frame}

\iffalse
\begin{frame}
\frametitle{Lessons learned from examining the performance of strategies}
\begin{itemize}
\item<1-> One strategy performing better for one player than for another does not mean this player has a winning strategy.
\item<2-> Strategies which are good for some $n$ may be horrendously bad for other $n$.
\item<3-> Modifying strategies may not always produce the expected outcome.
% Because preventing the alternating strategy from losing can worsen it even more. For example, for n=6 we see that modifying the alternation strategy makes it worse for A but B becomes better off.
\end{itemize}
\end{frame}
\fi

\begin{frame}
\frametitle{Describing the winning strategies}
\begin{itemize}
\item<1-> \plr B has a winning strategy for $n=2$ which we stumbled upon one description of when testing strategies:
\begin{itemize}
\item<2-> At the first move, place the same as the opponent.
\item<3-> At the second move, place the opposite of the opponent.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Describing the winning strategies}
\begin{itemize}
\item<1-> \plr A has a winning strategy for $n=4$ and $n=6$, but we don't (yet) have a nice description for it.
\item<2-> We can assume the first symbol to be placed is \sym0, so \plr A's first move is fixed.
\item<3-> An idea is to test if more of the moves can be fixed.
\begin{itemize}
\item<4-> For $n=4$, \plr A moves at most 10 times and it turns out that the first three moves are fixed (\sym0, \sym1, \sym0).
\item<5-> The strategy may be similar to the previously considered alternating strategy.
\item<6-> This works for all moves except no. 4, 5 and 6.
\item<7-> We resort to manually checking the exceptions.
% It turns out that they are quite simple.
% Note that there are two ways of ways of fixing the exceptions, since we right now can equivalently describe the strategy as either placing 0,1,0,1,0,1,... or as placing a zero and in all other moves placing the opposite of before.
% ACTION: Show code.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Winning strategy for \plr A for $n=4$}
\begin{itemize}
\item<1-> The result of the preceding steps were the following winning strategy:
\begin{itemize}
\item<2-> Start by playing \sym0 in your first turn and then always play the opposite of your previous turn except if:
\begin{itemize}
\item<2-> The game is \sym{011000} or \sym{00100111}, then copy your previous move.
\item<2-> It is the sixth move, play 0 if and only if the alternating sum of \plr B's previous four moves is zero.
\end{itemize}
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{About \plr A's winning strategy for $n=6$}
\begin{itemize}
\item<1-> The first five moves can be fixed as \sym0, \sym1, \sym0, \sym1, \sym0.
\begin{itemize}
\item<2-> Similarly, the last three moves also works by just inverting the previous ones.
\end{itemize}
\item<3-> The next moves requires handling some special cases.
\begin{itemize}
\item<4-> Move no. 6 and 7 only requires one special case each.
\item<5-> Move no. 8 requires five special cases.
\item<6-> Move no. 9 requires six special cases.
\item<7-> Move no. 10 requires 11 special cases.
\end{itemize}
\item<8-> This did not seem to lead to anything useful, and no patterns in the special cases were found.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Determining a winning strategy for $n=8$}
\begin{itemize}
\item<1-> It may seem obvious to use the information in the preceding discussion to attack this case.
\item<2-> This does not yield a conclusion\dots
\item<3-> \dots{}but we may get some indication that \plr A has a winning strategy for $n=8$.
\end{itemize}
\end{frame}

\end{document}
