#include "common.hpp"

static void appendStrategy(Strategy s, std::string name, bool a, bool b,
                           std::vector<StrategyInfo>& vs)
{
    vs.push_back({});
    vs.back().strategy = s;
    vs.back().name = name;
    vs.back().validForA = a;
    vs.back().validForB = b;
}

const StrategyInfo* findStrategy(std::string name,
                                 const std::vector<StrategyInfo>& strategies)
{
    for (size_t i = 0; i < strategies.size(); ++i)
    {
        if (strategies[i].name == name) return &strategies[i];
    }
    return nullptr;
}

std::vector<StrategyInfo> getStrategies()
{
    std::vector<StrategyInfo> strats;
    appendStrategy([](Bitstring& game, size_t)
    {
        return game.size() ? !game.back() : false;
    }, "Flip", true, true, strats);

    appendStrategy([](Bitstring& game, size_t)
    {
        if (((game.size()-1)/2)%2 == 0)
        {
            return !!game.back();
        }
        return !game.back();
    }, "B-Copy-flip", false, true, strats);

    appendStrategy([](Bitstring& game, size_t wordLen)
    {
        if (causesGameEnd(false, game, wordLen)) return true;
        return false;
    }, "Lowest binary", true, true, strats);

    // XOR of entire game.
    appendStrategy([](Bitstring& game, size_t)
    {
        return std::accumulate(game.begin(), game.end(), 0ULL) % 2 == 0;
    }, "Digit sum", true, true, strats);

    // Formerly "jens".
    appendStrategy([](Bitstring& game, size_t)
    {
        size_t ones = std::accumulate(game.begin(), game.end(), 0ULL);
        size_t zeroes = game.size() - ones;
        return zeroes >= ones;
    }, "Keep total balanced.", true, true, strats);

    appendStrategy([](Bitstring& game, size_t wordLen)
    {
        auto parity = game.size() % 2;
        bool result = false;
        for (size_t i = 0; i < game.size(); ++i)
        {
            if (i % 2 == parity)
            {
                result = result ^ game[i];
            }
        }
        if (causesGameEnd(result, game, wordLen)) return !result;
        return result;
    }, "XOR self", true, true, strats);


    appendStrategy([](Bitstring& game, size_t)
    {
        if (game.size() < 2) return false;
        size_t symbols[2] = {0, 0};
        for (size_t i = 0; i < 2; ++i)
        {
            symbols[i%2] += game[i];
        }
        return bool((symbols[0] % 2) ^ (symbols[1] % 2));
    }, "Silly", true, true, strats);

    appendStrategy([](Bitstring&, size_t)
    {
        return false;
    }, "Very silly", true, true, strats);

    appendStrategy([](Bitstring& game, size_t wordLen)
    {
        size_t ones = std::accumulate(game.begin(), game.end(), 0ULL);
        size_t zeroes = game.size() - ones;
        return zeroes <= wordLen + ones;
    }, "Modified keep total balanced", true, true, strats);

    appendStrategy([](Bitstring& game, size_t wordLen)
    {
        if (game.size() < 2) return false;
        if (causesGameEnd(!game[game.size()-2], game, wordLen)) return bool(game[game.size()-2]);
        return !game[game.size()-2];
    }, "Alternate", true, true, strats);

    appendStrategy([](Bitstring& game, size_t wordLen)
    {
        if (game.size() < 2) return false;
        size_t moveNo = game.size() / 2;
        bool symbol = moveNo % 2 ? !game[game.size() - 2] : game[game.size() - 2];
        if (causesGameEnd(symbol, game, wordLen)) return !symbol;
        return symbol;
    }, "Doubly alternate", true, true, strats);

    appendStrategy([](Bitstring& game, size_t wordLen)
    {
        if (game.size() < 2) return false;
        bool symbol = !bool(bool(game[game.size()-2])^bool(game[game.size()-1]));
        if (causesGameEnd(symbol, game, wordLen)) return !symbol;
        return symbol;
    }, "Prev XOR", true, true, strats);

    appendStrategy([](Bitstring& game, size_t wordLen)
    {
        size_t scores[] = {0, 0};
        for (size_t i = 0; i < game.size(); ++i)
        {
            scores[i%2] += game[i];
        }
        bool symbol = scores[game.size()%2] < scores[!(game.size()%2)];
        if (causesGameEnd(symbol, game, wordLen)) return !symbol;
        return symbol;
    }, "Keep player-sum balanced", true, true, strats);

    appendStrategy([](Bitstring& game, size_t)
    {
        if (!game.size()) return false;
        if ((game.size()/2)%2 == 0)
        {
            return bool(game.back());
        }
        return bool(1-game.back());
    }, "A-Copy-flip", true, false, strats);
    appendStrategy([](Bitstring& game, size_t)
    {
        if (game.size() < 2) return false;
        size_t move = game.size() / 2;
        bool invert = false;
        if (move == 3) invert = game == "011000";
        if (move == 4) invert = game == "00100111";
        if (move == 5) return game[3]-game[5]+game[7]-game[9] != 0;
        return invert ? !!game[game.size()-2] : !game[game.size()-2];
    }, "Win-4", true, true, strats);

    return strats;
}

