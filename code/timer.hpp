#include <iostream>
#include <iomanip>
#include <chrono>
#include <string>

template <typename Seconds = double>
class Timer
{
public:
    void start()
    {
        m_start = std::chrono::high_resolution_clock::now();
    }
    void stop()
    {
        m_stop = std::chrono::high_resolution_clock::now();
    }
    Seconds duration()
    {
        std::chrono::duration<Seconds> dur(m_stop - m_start);
        return dur.count();
    }
public:
    using Timepoint = decltype(std::chrono::high_resolution_clock::now());
    Timepoint m_start;
    Timepoint m_stop;
};

class ScopeTimer
{
public:
    ScopeTimer(const std::string& s) : m_msg{s}
    {
        m_timer.start();
    }
    ~ScopeTimer()
    {
        m_timer.stop();
        std::cout << "Timer \"" << m_msg << "\": " << m_timer.duration() << "s\n";
    }
private:
    Timer<double> m_timer;
    std::string m_msg;
};

