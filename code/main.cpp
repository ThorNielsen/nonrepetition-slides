#include <cmath>
#include <memory>
#include <random>
#include <stdexcept>

#include "common.hpp"
#include "timer.hpp"

Strategy createStartStrategy(Bitstring moves)
{
    return [moves](Bitstring& game, size_t)
    {
        size_t move = game.size() / 2;
        if (move >= moves.size()) return false;
        return bool(moves[move]);
    };
}

void findInitialAMoves(size_t n)
{
    if (n%2)
    {
        throw std::runtime_error("A does not have a winning strategy for odd n.");
    }
    Bitstring game;
    // Note that we can assume that A always starts by putting a '0', since the
    // case where a '1' is placed at the beginning is symmetrical (we can just
    // flip the labels of all the symbols).
    size_t maxMoves = std::pow(2, n-1) + n/2;
    std::unique_ptr<bool[]> useAt(new bool[maxMoves]);
    std::vector<Bitstring> validStarts;
    validStarts.push_back({0});
    while (!validStarts.empty())
    {
        Bitstring startMoves = validStarts.back();
        validStarts.pop_back();
        for (size_t i = 0; i < maxMoves; ++i)
        {
            // Initialise such that the parts where the strategy is used is
            // exactly where it is defined.
            useAt[i] = i <= startMoves.size();
        }
        for (size_t symbol = 0; symbol < 2; ++symbol)
        {
            TryPlacing _(startMoves, symbol);
            auto start = createStartStrategy(startMoves);
            bool isWinning = isPartOfWinningStrategy(game, n, &start, 0, useAt.get());
            std::cout << "For n=" << n
                      << ", does A have a winning strategy using initial moves "
                      << startMoves << ": " << (isWinning ? "Yes" : "No")
                      << "\n";
            if (isWinning)
            {
                validStarts.push_back(startMoves);
            }
        }
    }
}

void testBeginning(std::string aBegin, std::string bBegin, size_t n)
{
    Bitstring game = mergeToGame(aBegin, bBegin);
    std::cout << "For beginning " << game << ", winning strategy is attained by: ";
    std::cout << (whoHasWinningStrategy(game, n) ? "B" : "A") << "\n";
}

void stratTest4()
{
    std::cout << "===== TEST FOR n=4 =====\n";
    testBeginning("0", "0", 4);
    testBeginning("0", "1", 4);
    std::cout << "Strategy prescribes '1'. We test if '0' works in any case.\n";
    testBeginning("00", "0", 4);
    testBeginning("00", "1", 4);
    std::cout << "So A must start with '01'.\n";
    std::cout << "Strategy prescribes '0'. We test if '1' works in any case.\n";
    testBeginning("011", "00", 4);
    testBeginning("011", "01", 4);
    testBeginning("011", "10", 4);
    testBeginning("011", "11", 4);
    std::cout << "So A must start with '010'.\n";
    std::cout << "Strategy prescribes '1'. We test if '0' works in any case.\n";
    testBeginning("0101", "000", 4);
    testBeginning("0101", "001", 4);
    testBeginning("0101", "010", 4);
    testBeginning("0101", "011", 4);
    testBeginning("0101", "100", 4);
    testBeginning("0101", "101", 4);
    testBeginning("0101", "110", 4);
    testBeginning("0101", "111", 4);
    std::cout << "And the picture is now more complicated (as expected).\n";
}

void stratTest6()
{
    std::cout << "===== TEST FOR n=6 =====\n";
    testBeginning("0", "0", 6);
    testBeginning("0", "1", 6);
    std::cout << "Strategy prescribes '1'. We test if '0' works in any case.\n";
    testBeginning("00", "0", 6);
    testBeginning("00", "1", 6);
    std::cout << "So A must start with '01'.\n";
    std::cout << "Strategy prescribes '0'. We test if '1' works in any case.\n";
    testBeginning("011", "00", 6);
    testBeginning("011", "01", 6);
    testBeginning("011", "10", 6);
    testBeginning("011", "11", 6);
    std::cout << "So we can in one case start with 011 (if B plays 10).\n";
}

void flipTime()
{
    auto strategies = getStrategies();
    auto flipStrategy = findStrategy("Flip", strategies);
    Bitstring game;
    size_t rep[] = {0, 100000, 100000, 100000, 100000, 100000,
                    100000, 1000, 100000, 10, 100000};
    for (size_t n = 1; n <= 10; ++n)
    {
        Timer<double> timer;
        timer.start();
        bool res;
        for (size_t r = 0; r < rep[n]; ++r)
        {
            res = proveStrategy(game, n, &flipStrategy->strategy, 1);
        }
        timer.stop();
        std::cout << "For n=" << n << ", B-flip result is " << res << " (time: " << timer.duration()/rep[n] << ")\n";
    }
}

void initialFixed()
{
    stratTest4();
    stratTest6();
}

void bruteTime()
{
    Bitstring game;
    for (size_t i = 1; i < 7 && false; ++i)
    {
        size_t repetitions[] = {0, 1000000, 100000, 10000, 1000, 100, 10};
        Timer<double> timer;
        timer.start();
        for (size_t rep = 0; rep < repetitions[i]; ++rep)
        {
            whoHasWinningStrategy(game, i, 2);
        }
        timer.stop();
        std::cout << "n=" << i << ", s=2: " << std::setprecision(8)
                  << timer.duration() / repetitions[i] << "\n";
    }
}

void a8StrategyConstructor()
{
    Strategy testMe = [](Bitstring& game, size_t)
    {
        if (game.size() < 2) return false;
        return !game[game.size()-2];
    };
    // We wish to test a strategy for n=8. Therefore we should have 2^(n-1)+n/2
    // = 128+4 = 132 = 6*22 elements in the array.
    //                      0  1  2  3  4  5  6  7  8  9
    bool useStrategyAt[] =
    {
    //  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    };
    Bitstring game;
    std::cout << "A8 strategy with first 22*1+12=34 moves fixed: " << std::flush;
    std::cout << isPartOfWinningStrategy(game, 8, &testMe, 0, useStrategyAt) << "\n";
    useStrategyAt[22*1+11] = 0;
    std::cout << "A8 strategy with first 22*1+11=33 moves fixed: " << std::flush;
    std::cout << isPartOfWinningStrategy(game, 8, &testMe, 0, useStrategyAt) << "\n";
}

void a6StrategyConstructor()
{
    Strategy testMe = [](Bitstring& game, size_t)
    {
        if (game.size() < 2) return false;
        size_t move = game.size() / 2;
        bool invert = false;
        if (move == 5) invert = game == "0011001001";
        if (move == 6) invert = game == "011100110110";
        if (move == 7)
        {
            if (game == "00100011001001" || game == "00110010001001" ||
                game == "01100011001001" || game == "01100111001000" ||
                game == "01100111011000") invert = true;
        }
        if (move == 8)
        {
            if (game == "0010011000110111" || game == "0010011100110110" ||
                game == "0110011100100001" || game == "0110011101100001" ||
                game == "0111001101110110" || game == "0111011100110110")
            {
                invert = true;
            }
        }
        if (move == 9)
        {
            if (game == "001001100011011110" ||
                game == "001100100101011101" ||
                game == "001101100011001001" ||
                game == "001101100111001000" ||
                game == "011000100011001001" ||
                game == "011000110010001001" ||
                game == "011001110010000110" ||
                game == "011001110110000110" ||
                game == "011001110111001000" ||
                game == "011101100011001001" ||
                game == "011101100111001000")
            {
                invert = true;
            }

        }
        return invert ? !(move % 2) : !!(move % 2);
    };
    // We wish to test a strategy for n=6. Therefore we should have 2^(n-1)+n/2
    // = 2^5+3 = 35 elements in the array.
    //                      0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
    bool useStrategyAt[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    //                     18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1};
    Bitstring game;
    constexpr int debugTurns = 10;
    std::cout << isPartOfWinningStrategy<2*(debugTurns+1)>(game, 6, &testMe, 0, useStrategyAt) << "\n";
}

void a4StrategyConstructor()
{
    Strategy testMe = [](Bitstring& game, size_t)
    {
        if (game.size() < 2) return false;
        size_t move = game.size() / 2;
        //if (move == 3) return !beginsWith(game, "011000");
        //if (move == 4) return beginsWith(game, "011000") || game == "00100111";
        //if (move == 5) return game[3]-game[5]+game[7]-game[9] != 0;
        //return !game[game.size()-2];
        return bool(move % 2);
    };
    // We wish to test a strategy for n=4. Therefore we should have 2^(n-1)+n/2
    // = 8+2 = 10 elements in the array.
    //                      0  1  2  3  4  5  6  7  8  9
    bool useStrategyAt[] = {1, 1, 1, 0, 0, 0, 0, 0, 0, 0};
    Bitstring game;
    constexpr int debugTurns = 3;
    std::cout << isPartOfWinningStrategy<2*(debugTurns+1)>(game, 4, &testMe, 0, useStrategyAt) << "\n";
}

void a4StrategyLastMove()
{
    Strategy testMe = [](Bitstring& game, size_t)
    {
        if (game.size() < 2) return false;
        size_t move = game.size() / 2;
        bool invert = false;
        if (game == "011000" || game == "00100111") invert = true;
        if (move == 5) return game[3]-game[5]+game[7]-game[9] != 0;
        return invert ? !!game[game.size() - 2] : !game[game.size()-2];
    };
    // We wish to test a strategy for n=4. Therefore we should have 2^(n-1)+n/2
    // = 8+2 = 10 elements in the array.
    //                      0  1  2  3  4  5  6  7  8  9
    bool useStrategyAt[] = {1, 1, 1, 1, 1, 0, 0, 0, 0, 0};
    Bitstring game;
    constexpr int debugTurns = 5;
    std::cout << isPartOfWinningStrategy<2*(debugTurns+1), false>(game, 4, &testMe, 0, useStrategyAt) << "\n";

    std::cout << "--------------------------\n";

    // Now find all that should be distinguished between.
    std::cout << isPartOfWinningStrategy<2*(debugTurns+1), true>(game, 4, &testMe, 0, useStrategyAt) << "\n";
}

int main()
{
    a4StrategyLastMove();
    return 0;

    a4StrategyLastMove();
    pause();
    auto strategies = getStrategies();
    pause("4-strategy test.");
    testStrategies(strategies, 4);
    pause("6-strategy test.");
    testStrategies(strategies, 6);

    pause("A4 initial moves.");
    findInitialAMoves(4);
    pause("A6 initial moves.");
    findInitialAMoves(6);

    pause("Test initial configurations.");
    stratTest4();
    stratTest6();

    pause("A4-strategy.");
    a4StrategyConstructor();
    pause("A6-strategy.");
    a6StrategyConstructor();

    pause("A8 strategy construction.");
    a8StrategyConstructor();
}

