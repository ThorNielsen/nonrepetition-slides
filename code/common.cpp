#include "common.hpp"

#include <cmath>
#include <memory>
#include <random>

std::mt19937 rgen(0xdeadbeef);
std::uniform_int_distribution<char> rdist(0, 1);

std::ostream& operator<<(std::ostream& ost, const ExtendedProofInfo& epi)
{
    std::string fmt;
    double percentage = 100.0 * (epi.gamesWon / (double)epi.totalGames);
    if (percentage < 40) /* Don't change format. */;
    else if (percentage < 80) fmt += fgYellow();
    else if (percentage < 90) fmt += fgPurple();
    else if (percentage < 99) fmt += bold() + fgGreen();
    else fmt += bold() + fgPurple();
    if (epi.gamesWon == epi.totalGames) fmt = bold() + fgRed();

    ost << "(won " << epi.gamesWon << " of " << epi.totalGames
        << " games, " << fmt << percentage << "%" << resetFmt() << ")";
    return ost;
}


bool gameEnded(const Bitstring& game, size_t wordLen)
{
    if (game.size() <= wordLen) return false;
    for (size_t i = 0; i + wordLen < game.size(); ++i)
    {
        if (std::equal(game.end() - wordLen, game.end(), game.begin() + i)) return true;
    }
    return false;
}

// Returns:
// false -> A has a winning strategy.
// true  -> B has a winning strategy.
bool whoHasWinningStrategy(Bitstring& game, char wordLen, char symCount)
{
    // Since each player plays exactly one symbol each turn, the parity of the
    // game length (number of symbols played so far) will give us which player
    // should play the next symbol -- if it is even, (=0 mod 2) then the current
    // player is A, otherwise the current player is B.
    bool currPlayer = game.size() % 2;
    // *IF* the game has ended, it did when the preceeding player placed its
    // symbol. Therefore the preceeding player lost and the one before that won.
    // But as there are only two players, that player is the current one.
    if (gameEnded(game, wordLen)) return currPlayer;
    // The player now has to choose between the different symbols.
    for (char i = 0; i < symCount; ++i)
    {
        // Try placing one of them and test it.
        TryPlacing move(game, i);
        if (whoHasWinningStrategy(game, wordLen, symCount) == currPlayer)
        {
            // In this case, we have that the resulting game gives the current
            // player a winning strategy.
            return currPlayer;
        }
        // However if we didn't get a winning strategy that does NOT imply the
        // other player has a winning strategy -- it would be possible to get a
        // winning game if we place a different symbol. So that is what we do.

        // Note that at the end of this loop there's an implicit removal of the
        // speculatively placed symbol.
    }
    // Now we have exhausted all possible symbols we could place, and we were
    // never able to get a winning strategy. This must mean that the other
    // player has one instead.
    return !currPlayer;
}



// Return value: Whether the strategy will always make the given player win.
// Note: The strategy MUST be deterministic, otherwise the proof will be
// invalid.
// Note: This function is NOT very optimised (it will test all possible games to
// collect winning statistics), so it is not suitable for proving whether a
// strategy is a winning one in general.
bool proveStrategy(ExtendedProofInfo* info)
{
    bool currPlayer = info->game.size() % 2;
    if (gameEnded(info->game, info->wordLen))
    {
        // If the game has ended then the player just before have won the game.
        // As there are only two players this is equivalent to saying that the
        // player 'after' the losing player is the winner.
        if (currPlayer == info->player) ++info->gamesWon;
        ++info->totalGames;
        return currPlayer == info->player;
    }
    if (currPlayer == info->player)
    {
        // Current player only plays according to strategy.
        TryPlacing thisPlayerMove(info->game,
                                  (*info->strategy)(info->game, info->wordLen));
        return proveStrategy(info);
    }
    bool isCurrentWinner = true;
    // We test all the possible moves the other player can make to see if one of
    // them can somehow cause the current player to lose.
    for (char i = 0; i < 2; ++i)
    {
        TryPlacing otherPlayerMove(info->game, i);
        if (!proveStrategy(info))
        {
            isCurrentWinner = false;
        }
    }
    return isCurrentWinner;
}

// Return value: Whether the strategy will always make the given player win.
// Note: The strategy MUST be deterministic, otherwise the proof will be
// invalid.
bool proveStrategy(Bitstring& game, size_t wordLen,
                   const Strategy* strategy, bool player)
{
    bool currPlayer = game.size() % 2;
    if (gameEnded(game, wordLen))
    {
        return currPlayer == player;
    }
    if (currPlayer == player)
    {
        TryPlacing thisPlayerMove(game, (*strategy)(game, wordLen));
        return proveStrategy(game, wordLen, strategy, player);
    }
    for (char i = 0; i < 2; ++i)
    {
        TryPlacing otherPlayerMove(game, i);
        if (!proveStrategy(game, wordLen, strategy, player))
        {
            return false;
        }
    }
    return true;
}

// Simulates a game using any strategies (including probabilistic ones) for A
// and B. Returns whether B wins, that is:
// false = A wins
// true  = B wins
bool runGame(size_t wordLen, Strategy* a, Strategy* b)
{
    Bitstring game;
    while (!gameEnded(game, wordLen))
    {
        Strategy* currPlayer = game.size() % 2 ? b : a;
        game.push_back((*currPlayer)(game, wordLen));
    }
    // At the end of the loop, the losing player has made the final move, and
    // therefore the losing one can be determined by the parity of the game
    // length - if it is even then A and B has made the same number of moves,
    // which means the last player was B since A started, so in this case A has
    // won, but if the parity is odd then B has won.
    return game.size() % 2 == 1;
}



Bitstring toGame(std::string s)
{
    Bitstring b;
    for (auto c : s)
    {
        b.push_back(c-'0');
    }
    return b;
}

Bitstring mergeToGame(std::string aMoves, std::string bMoves)
{
    std::string s;
    size_t at = 0;
    while (at < aMoves.size() && at < bMoves.size())
    {
        s.push_back(aMoves[at]);
        s.push_back(bMoves[at]);
        ++at;
    }
    if (at < aMoves.size())
    {
        // We have copied all there is from B, but we can still get one element
        // more from A.
        s.push_back(aMoves[at]);
    }
    return toGame(s);
}


void testStrategies(const std::vector<StrategyInfo>& strategies, size_t n)
{
    Bitstring game;
    std::cout << "Parameters: n=" << n
              << ", symbols=2 (game length upper bound: "
              << (std::pow(2, n)+n) << ")"
              << std::endl;
    for (auto& strategy : strategies)
    {
        auto winMsg = bold() + fgGreen() + "WINNING!" + resetFmt();
        if (strategy.validForA)
        {
            ExtendedProofInfo info{n, 0, 0, {}, &strategy.strategy, false};
            bool win = proveStrategy(&info);
            std::cout << "A-'" << strategy.name << "': ";
            std::cout << (win ? winMsg : "No.");
            std::cout << " " << info << "\n";
        }
        if (strategy.validForB)
        {
            ExtendedProofInfo info{n, 0, 0, {}, &strategy.strategy, true};
            bool win = proveStrategy(&info);
            std::cout << "B-'" << strategy.name << "': ";
            std::cout << (win ? winMsg : "No.");
            std::cout << " " << info << "\n";
        }
    }
    std::cout << std::endl;

    // A basic sanity check: If we somehow end up with a non-empty game
    // after testing our strategies then something adds symbols to the game
    // without removing them afterwards.
    if (!game.empty())
    {
        throw std::runtime_error("Non-empty game after testing!");
    }
}

