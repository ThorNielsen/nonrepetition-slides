#include "common.hpp"

static bool formatting = true;

void enableFormatting() { formatting = true; }
void disableFormatting() { formatting = false; }
bool formattingEnabled() { return formatting; }

std::string fgRed() { return formatting ? "\033[31m" : ""; }
std::string fgGreen() { return formatting ? "\033[32m" : ""; }
std::string fgYellow() { return formatting ? "\033[33m" : ""; }
std::string fgBlue() { return formatting ? "\033[34m" : ""; }
std::string fgPurple() { return formatting ? "\033[35m" : ""; }
std::string fgCyan() { return formatting ? "\033[36m" : ""; }
std::string bold() { return formatting ? "\033[1m" : ""; }
std::string resetFmt() { return formatting ? "\033[0m" : ""; }

