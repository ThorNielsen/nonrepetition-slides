#include <algorithm>
#include <functional>
#include <iostream>
#include <numeric>
#include <random>
#include <utility>
#include <vector>

extern std::mt19937 rgen;
extern std::uniform_int_distribution<char> rdist;

// Note: Although this is technically called a "Bitstring", it may in general
// contain more symbols, if that is desired. On most computers it can contain
// symbols in the range [-128, 127].
using Bitstring = std::vector<char>;

// This is custom data type which is used to try to place a symbol and ensuring
// that it is removed again afterwards. It places the symbol at the time of
// declaration (being written in the code) and it removes the symbol when it
// reaches a '}' which corresponds to the '{' immediately preceding the
// declaration.
class TryPlacing
{
public:
    // This is called upon construction of an element of this type and simply
    // adds the given symbol to the bitstring and makes a note of where it added
    // that element.
    TryPlacing(Bitstring& bs, Bitstring::value_type val)
    : bs_(&bs)
    {
        bs.push_back(val);
    }

    // This is called when an element of this type is destroyed, that is
    // whenever the element is not visible anymore. That could be at the end of
    // a function (preceding a return), at the end of a loop, etc.
    ~TryPlacing()
    {
        bs_->pop_back();
    }
private:
    Bitstring* bs_;
};

// A strategy should take the current game and word length, and return the
// chosen symbol.
using Strategy = std::function<bool(Bitstring&, size_t)>;

struct StrategyInfo
{
    Strategy strategy;
    std::string name;
    bool validForA;
    bool validForB;
};

struct ExtendedProofInfo
{
    size_t wordLen;
    size_t gamesWon;
    size_t totalGames;
    Bitstring game;
    const Strategy* strategy;
    bool player;
};

inline Bitstring extractMoves(Bitstring game, bool player)
{
    Bitstring bs;
    for (size_t i = 0; i < game.size(); ++i)
    {
        if (i%2 == player) bs.push_back(player);
    }
    return bs;
}

void enableFormatting();
void disableFormatting();
bool formattingEnabled();

std::string fgRed();
std::string fgGreen();
std::string fgYellow();
std::string fgBlue();
std::string fgPurple();
std::string fgCyan();
std::string bold();
std::string resetFmt();

inline std::ostream& operator<<(std::ostream& ost, const Bitstring& bs)
{
    std::string colours[2] = {fgGreen(), fgBlue()};
    size_t player = 0;
    for (auto& b : bs)
    {
        ost << colours[(player++) % 2];
        ost << (int)b;
    }
    ost << resetFmt();
    return ost;
}

inline bool operator==(const Bitstring& bs, const std::string& s)
{
    if (bs.size() != s.size()) return false;
    for (size_t i = 0; i < bs.size(); ++i)
    {
        if (bs[i]+'0' != s[i]) return false;
    }
    return true;
}

inline bool operator==(const std::string& s, const Bitstring& bs)
{
    return bs == s;
}

inline bool operator!=(const Bitstring& bs, const std::string& s)
{
    return !(bs == s);
}

inline bool operator!=(const std::string& s, const Bitstring& bs)
{
    return !(bs == s);
}

inline bool beginsWith(const Bitstring& bs, const std::string& s)
{
    if (bs.size() < s.size()) return false;
    for (size_t i = 0; i < s.size(); ++i)
    {
        if (bs[i]+'0' != s[i]) return false;
    }
    return true;
}

inline void pause(std::string msg = "")
{
    std::cout << msg;
    std::cin.get();
}

std::ostream& operator<<(std::ostream& ost, const ExtendedProofInfo& epi);

// This function tests whether the given game has just ended (that is, whether
// the last move has caused a game end). It does NOT work if the game has been
// continued - e.g. if n=1, game=00 will return true while n=1, game=001 may
// return anything.
bool gameEnded(const Bitstring& game, size_t wordLen);

bool whoHasWinningStrategy(Bitstring& game, char wordLen, char symCount = 2);

bool proveStrategy(ExtendedProofInfo* info);

bool proveStrategy(Bitstring& game, size_t wordLen,
                   const Strategy* strategy, bool player);

bool runGame(size_t wordLen, Strategy* a, Strategy* b);

void testStrategies(const std::vector<StrategyInfo>& strategies, size_t n);

Bitstring toGame(std::string s);
Bitstring mergeToGame(std::string aMoves, std::string bMoves);

// Test whether a given move causes the game to end.
inline bool causesGameEnd(bool move, Bitstring& game, size_t wordLen)
{
    TryPlacing guard(game, move);
    return gameEnded(game, wordLen);
}




std::vector<StrategyInfo> getStrategies();

const StrategyInfo* findStrategy(std::string name,
                                 const std::vector<StrategyInfo>& strategies);




// This tests whether performing the moves prescribed by the strategy will still
// cause the given player to have a winning strategy. For example for n=3 we
// know that B has a winning strategy, but does this still hold if player B
// uses a specific strategy for some of its moves? For example, what if B
// always placed a zero in its first move?
// IMPORTANT: useStrategy MUST contain at least ceil(2^n+n) elements, where
// n is the word length. Equivalently at least 2^(n-1) + ceil(n/2) elements.
// That array tells at which moves the strategy should be used -- for example if
// player=B and useStrategy={0,0,1,0,1,0,0,...,0} then the strategy will be used
// whenever B makes its third and fifth move, which will be the sixth and tenth
// move overall.
// Note that this also takes two compile-time parameters. This is done to allow
// the compiler to remove any unnecessary comparisons and reduce the number of
// parameters to the function.
template <size_t debugUntil = 0, bool includeExpected = false>
bool isPartOfWinningStrategy(Bitstring& game, size_t wordLen,
                             const Strategy* strategy, bool player,
                             bool useStrategy[])
{
    size_t move = game.size()/2;
    bool currPlayer = game.size() % 2;
    if (gameEnded(game, wordLen))
    {
        return currPlayer == player;
    }
    if (useStrategy[move] && currPlayer == player)
    {
        auto move = (*strategy)(game, wordLen);
        TryPlacing thisPlayerMove(game, move);
        return isPartOfWinningStrategy<debugUntil, includeExpected>
                                      (game, wordLen, strategy, player, useStrategy);
    }

    bool flip = rdist(rgen);
    flip = false;

    bool didItWork = false;
    for (char it = 0; it < 2; ++it)
    {
        char i = it ^ flip;
        TryPlacing playerMove(game, i);
        if (currPlayer == player)
        {
            // In this case, the current move is not prescribed by the strategy
            // and we must therefore check if we can make any move which causes
            // us to have a winning strategy.
            if (whoHasWinningStrategy(game, wordLen) != player) continue;
        }
        if (!isPartOfWinningStrategy<debugUntil, includeExpected>
                                    (game, wordLen, strategy, player, useStrategy))
        {
            // If someone other than ourselves can make us lose then we do NOT
            // have a winning strategy.
            if (currPlayer != player)
            {
                return false;
            }
            // Otherwise, we may still have a winning strategy if we place
            // another symbol. So wo just continue the loop and hope for the
            // best.
        }
        else
        {
            // If we make a move which gives us a winning strategy, we then know
            // the given configuration gives us a winning strategy.
            if (currPlayer == player)
            {
                if (game.size() < debugUntil)
                {
                    didItWork = true;
                    bool expectedMove = move % 2;
                    Bitstring truncGame = game;
                    truncGame.pop_back();
                    bool mandatory = false;
                    if (!includeExpected && game.back() == expectedMove) continue;

                    {
                        TryPlacing _(truncGame, expectedMove);
                        if (whoHasWinningStrategy(truncGame, wordLen) != player)
                        {
                            mandatory = true;
                        }
                    }
                    if (mandatory)
                    {
                        std::cout << bold() << fgRed() << "MUST choose "
                                  << (int)game.back() << " when handling game "
                                  << truncGame;
                        if (causesGameEnd(expectedMove, truncGame, wordLen))
                            std::cout << "  *";
                        std::cout << resetFmt() << "\n";
                    }
                    else
                    {
                        std::cout << "Given " << truncGame
                                  << " choosing " << (int)game.back()
                                  << " works.\n";
                    }
                    // If we are debugging, we want to see what all possible
                    // moves lead to. So in that case we skip over the early
                    // return.
                    continue;
                }
                return true;
            }
        }
    }
    // And instead we return true here if the previous return was skipped due
    // to debugging.
    if (didItWork) return true;
    // In this case, the following may have happened:
    // * It is the opponent's turn, and he could not make any move which caused
    //   us to lose, so we have a winning strategy.
    // * It is our turn, and we could not make a move that caused us to have a
    //   winning strategy.
    // Therefore when reaching this point we have a winning strategy if and only
    // if it is the opponent's turn.
    return currPlayer != player;
}
